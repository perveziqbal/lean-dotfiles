;; prefer UTF8

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(defconst seartipy-cache-directory
  (expand-file-name (concat user-emacs-directory ".cache/"))
  "storage area for persistent files")

(unless (file-exists-p seartipy-cache-directory)
  (make-directory seartipy-cache-directory t))

(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-a-linux* (eq system-type 'gnu/linux))

(setq custom-file (expand-file-name "custom.el" seartipy-cache-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; misc

(when *is-a-mac*
  (setq mac-option-modifier 'meta))

(setq-default
 bookmark-default-file (expand-file-name ".bookmarks.el" seartipy-cache-directory)
 buffers-menu-max-size 30
 case-fold-search t
 column-number-mode t
 delete-selection-mode t
 indent-tabs-mode nil
 mouse-yank-at-point t
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 show-trailing-whitespace t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil
 visible-bell t)

(transient-mark-mode t)

(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)

(setq indicate-empty-lines t)
(tool-bar-mode -1)
(set-scroll-bar-mode nil)
(menu-bar-mode -1)

(add-hook 'term-mode-hook
          (lambda ()
            (setq line-spacing 0)))

(electric-indent-mode t)
(setq create-lockfiles nil)
(fset 'yes-or-no-p 'y-or-n-p)

;; do not ask follow link
(customize-set-variable 'find-file-visit-truename t)

(windmove-default-keybindings)
(winner-mode)
(cua-selection-mode t)

;; Don't disable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)
;; Don't disable case-change functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(show-paren-mode 1)

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")



;; MELPA

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; auto update packages
(use-package auto-package-update
  :ensure t
  :config
  (progn
    (when (and (apu--should-update-packages-p)
               (not (string= (getenv "CI") "true"))
               (y-or-n-p-with-timeout "Update packages?" 60 t))
      (auto-package-update-now))))

(use-package auto-compile
  :ensure t
  :config
  (progn
    (auto-compile-on-load-mode)
    (auto-compile-on-save-mode)))



;;;; UI

(use-package spacemacs-theme
  :defer t
  :ensure t)

(load-theme 'spacemacs-dark)

(use-package page-break-lines
  :ensure t
  :diminish page-break-lines-mode
  :init
  (global-page-break-lines-mode t))



;; Essential

(use-package savehist
  :init
  (progn
    (setq savehist-file (concat seartipy-cache-directory "savehist")
          enable-recursive-minibuffers t ; Allow commands in minibuffers
          history-length 1000
          savehist-additional-variables '(mark-ring
                                          global-mark-ring
                                          search-ring
                                          regexp-search-ring
                                          extended-command-history)
          savehist-autosave-interval 60)
    (savehist-mode t)))

(use-package saveplace
  :init
  (progn
    (setq save-place t
          save-place-file (concat seartipy-cache-directory "places"))))


(use-package recentf
  :config
  (setq recentf-save-file (concat seartipy-cache-directory "recentf"))
  (setq recentf-max-saved-items 100)
  (setq recentf-auto-save-timer (run-with-idle-timer 600 t 'recentf-save-list)))

(use-package noflet
  :ensure t
  :config
  ;; do not ask to kill processes
  (defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
    (noflet ((process-list ())) ad-do-it)))



;;;; keybindings management packages

(use-package bind-key
  :ensure t)

(use-package hydra
  :defer t
  :ensure t
  :config
  (progn
    (require 'hydra-examples)

    (defhydra hydra-font-zoom ()
      "font-zoom"
      ("+" text-scale-increase "inc")
      ("-" text-scale-decrease "dec")
      ("=" (text-scale-set 0) "reset")
      ("q" nil "quit"))

    (defhydra hydra-splitter ()
      "splitter"
      ("h" hydra-move-splitter-left "left")
      ("j" hydra-move-splitter-down "down")
      ("k" hydra-move-splitter-up "up")
      ("l" hydra-move-splitter-right "right")
      ("b" balance-windows "balance")
      ("q" nil "quit"))

    (defhydra hydra-error ()
      "goto-error"
      ("f" first-error "first")
      ("n" next-error "next")
      ("p" previous-error "prev")
      ("v" recenter-top-bottom "recenter")
      ("q" nil "quit"))

    (defhydra hydra-toggle (:color blue)
      "toggle"
      ("i" indent-guide-mode "indent guide")
      ("s" smartparens-strict-mode "smartparenss")
      ("n" neotree-toggle "neotree")
      ("t" multi-term-dedicated-toggle "multi term")
      ("g" golden-ratio-mode "golden ratio")
      ("h" global-hl-line-mode "highlight line")
      ("l" linum-mode)
      ("f" toggle-frame-fullscreen "fullscreen")
      ("m" toggle-frame-maximized "mazimized")
      ("d" toggle-debug-on-error "debug")
      ("w" whitespace-mode "whitespace")
      ("q" nil "cancel"))

    (bind-key "C-, z" 'hydra-font-zoom/body)
    (bind-key "C-, t" 'hydra-toggle/body)
    (bind-key "C-, e" 'hydra-error/body)
    (bind-key "C-, s" 'hydra-splitter/body)))

(use-package guide-key
  :ensure t
  :diminish guide-key-mode
  :config
  (progn
    (setq guide-key/guide-key-sequence '("C-c" "C-x" "C-,"))
    (setq guide-key/recursive-key-sequence-flag t)
    (setq guide-key/popup-window-position 'bottom)
    (guide-key-mode)))

(use-package neotree
  :ensure t
  :bind ("<f8>" . neotree-toggle))




;;;; IDO

(use-package ido
  :ensure t
  :init
  (progn
    (customize-set-variable 'ido-save-directory-list-file
                            (concat seartipy-cache-directory "ido-last"))
    (setq ido-enable-flex-matching t)
    (setq ido-use-filename-at-point nil)
    (setq ido-auto-merge-work-directories-length 0)
    (setq ido-use-virtual-buffers t)
    (setq ido-create-new-buffer 'always))
  :config
  (progn
    (ido-mode)
    (ido-everywhere)))

(use-package ido-ubiquitous
  :ensure t
  :config
  (ido-ubiquitous-mode))

;; Use smex to handle M-x
(use-package smex
  :ensure t
  :init
  ;; Change path for ~/.smex-items
  (setq smex-save-file (expand-file-name ".smex-items" seartipy-cache-directory))

  :config
  (bind-key [remap execute-extended-command] 'smex))

(use-package flx-ido
  :ensure t
  :config
  (flx-ido-mode))

(use-package ido-vertical-mode
  :ensure t
  :config
  (ido-vertical-mode)
  (setq ido-vertical-define-keys 'C-n-C-p-up-and-down))



;;;; Window management

(use-package window-numbering
  :ensure t
  :config
  (window-numbering-mode))

(use-package buffer-move
  :ensure t
  :bind( ( "<C-S-up>"    . buf-move-up)
         ( "<C-S-down>"  . buf-move-down)
         ( "<C-S-left>"  . buf-move-left)
         ( "<C-S-right>" . buf-move-right)))

(use-package golden-ratio
  :diminish " ⓖ"
  :bind ("C-c g" . golden-ratio-mode)
  :ensure t)

(use-package uniquify
  :init
  (setq uniquify-buffer-name-style 'post-forward-angle-brackets
        uniquify-ignore-buffers-re "^\\*"))



;; Search

(use-package avy
  :ensure t
  :bind ("C-'" . avy-goto-word-or-subword-1)
  :init
  (progn
    (setq avy-keys (number-sequence ?a ?z))
    (setq avy-background t)))

(use-package ag
  :ensure t
  :commands (ag ag-files ag-regexp ag-project ag-dired)
  :bind ("M-?" . ag-project)
  :config (setq ag-highlight-search t
                ag-reuse-buffers t))

(use-package wgrep
  :ensure t
  :config
  (progn
    (use-package wgrep-ag :ensure t)
    (autoload 'wgrep-ag-setup "wgrep-ag")
    (add-hook 'ag-mode-hook 'wgrep-ag-setup)))



;;;; Editing

(use-package whitespace-cleanup-mode
  :ensure t
  :diminish whitespace-cleanup-mode
  :config
  (global-whitespace-cleanup-mode))

(dolist (hook '(special-mode-hook
                Info-mode-hook
                eww-mode-hook
                term-mode-hook
                comint-mode-hook
                compilation-mode-hook
                magit-popup-mode-hook
                minibuffer-setup-hook))
  (add-hook hook (lambda ()
                   (setq show-trailing-whitespace nil))))

(bind-key [remap just-one-space] 'cycle-spacing)
(bind-key "RET" 'newline-and-indent)

(use-package move-dup
  :ensure t
  :bind (("<M-S-up>" . md/move-lines-up)
         ("<M-S-down>" . md/move-lines-down)))

(use-package easy-kill
  :ensure t
  :init
  (global-set-key [remap kill-ring-save] 'easy-kill))

(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))

(use-package multiple-cursors
  :ensure t
  :bind (("C-<" . mc/mark-previous-like-this)
         ("C->" . mc/mark-next-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("C-c c r" . set-rectangular-region-anchor)
         ("C-c c c" . mc/edit-lines)
         ("C-c c e" . mc/edit-ends-of-lines)
         ("C-c c a" . mc/edit-beginnings-of-lines)))

(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config
  (global-undo-tree-mode))

(use-package iedit
  :ensure t
  :diminish iedit-mode)



;;;; Git

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :init
  (setq-default
   magit-log-arguments '("--graph" "--show-signature")
   magit-process-popup-time 10
   magit-diff-refine-hunk t
   magit-push-always-verify nil)
  :config
  (global-git-commit-mode))

(use-package ediff
  :ensure t
  :init
  (progn
    (setq-default
     ediff-window-setup-function 'ediff-setup-windows-plain
     ediff-split-window-function 'split-window-horizontally
     ediff-merge-split-window-function 'split-window-horizontally)))



;;;; Programming

(use-package eldoc
  :diminish eldoc-mode
  :ensure t
  :commands (eldoc-mode)
  :config
  (progn
    (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode)
    (add-hook 'ielm-mode-hook #'eldoc-mode)))

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize)
  (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "JAVA_HOME" "JDK_HOME"))
    (add-to-list 'exec-path-from-shell-variables var)))

(use-package multi-term
  :ensure t
  :bind (("<f5>" . multi-term-dedicated-toggle))
  :commands (multi-term)
  :init
  (custom-set-variables '(multi-term-dedicated-select-after-open-p t))
  (setq multi-term-scroll-to-bottom-on-output "others")
  (setq multi-term-scroll-show-maximum-output +1)
  :config
  (progn
    (add-to-list 'term-bind-key-alist '("M-DEL" . term-send-backward-kill-word))
    (add-to-list 'term-bind-key-alist '("M-d" . term-send-forward-kill-word))))

(use-package flycheck
  :defer t
  :ensure t
  :diminish flycheck-mode
  :init
  (add-hook 'after-init-hook 'global-flycheck-mode)
  (setq flycheck-check-syntax-automatically '(save new-line mode-enabled)
        flycheck-idle-change-delay 0.8)
  (setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list))

(use-package company
  :diminish company-mode " ⓐ"
  :ensure t
  :commands (company-mode)
  :init
  (progn
    (setq company-idle-delay 0.5
          company-tooltip-limit 10
          company-minimum-prefix-length 2
          company-tooltip-flip-when-above t))
  :config
  (progn
    (setq company-minimum-prefix-length 2)
    (global-company-mode)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode " ⓟ"
  :commands (smartparens-mode smartparens-strict-mode)
  :init
  (progn
    (setq sp-show-pair-delay 0
          ;; no highlighting of region between parens
          sp-highlight-pair-overlay nil
          sp-highlight-wrap-overlay nil
          sp-highlight-wrap-tag-overlay nil

          sp-show-pair-from-inside nil
          sp-base-key-bindings 'paredit
          sp-autoskip-closing-pair 'always
          sp-hybrid-kill-entire-symbol nil
          sp-cancel-autoskip-on-backward-movement nil)
    (add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
    :config
    (progn
      (require 'smartparens-config)
      (sp-use-paredit-bindings)
      (show-smartparens-global-mode))))



;;;; Clojure

(use-package rainbow-delimiters
  :commands (rainbow-delimiters-mode)
  :diminish rainbow-delimiters-mode
  :ensure t)

(use-package aggressive-indent
  :commands (aggressive-indent-mode)
  :diminish aggressive-indent-mode " Ⓘ"
  :ensure t)

(use-package clojure-mode
  :ensure t
  :defer t
  :config
  (progn
    (defun clojure-mode-setup ()
      (subword-mode)
      (company-mode)
      (smartparens-strict-mode)
      (rainbow-delimiters-mode)
      (aggressive-indent-mode)
      (eldoc-mode))
    (add-hook 'clojure-mode-hook 'clojure-mode-setup)

    (add-hook 'after-save-hook 'check-parens nil t)
    (add-to-list 'auto-mode-alist '("\\.cljs\\'" . clojure-mode))))

(use-package clj-refactor
  :diminish clj-refactor-mode
  :ensure t
  :defer t
  :init
  (add-hook 'clojure-mode-hook 'clj-refactor-mode)
  :config
  (progn
    (cljr-add-keybindings-with-prefix "C-c C-f")))

(use-package cider
  :ensure t
  :commands (cider-jack-in cider-connect cider-jack-in-clojurescript)
  :init
  (progn
    (setq nrepl-popup-stacktraces nil)
    (setq cider-show-error-buffer nil)
    (setq cider-ovelays-use-font-lock t)
    (setq nrepl-buffer-name-show-port t)
    (setq cider-prompt-save-file-on-load nil)

    (setq cider-repl-use-clojure-font-lock t)
    (setq cider-repl-wrap-history t)
    (setq cider-repl-pop-to-buffer-on-connect nil)

    (defun cider-repl-mode-setup ()
      (subword-mode)
      (company-mode)
      (smartparens-strict-mode)
      (rainbow-delimiters-mode)
      (setq show-trailing-whitespace nil))

    (add-hook 'cider-repl-mode-hook 'cider-repl-mode-setup)))



;;;;    Web

(use-package json-mode
  :ensure t
  :mode "\\.json\\'")

(use-package js2-mode
  :ensure t
  :mode "\\.js\\'"
  :init
  (progn
    (setq-default js2-mode-show-parse-errors nil
                  js2-mode-show-strict-warnings nil
                  js2-strict-missing-semi-warning nil
                  js2-basic-offset 2)
    (add-hook 'js2-mode-hook (lambda () (setq mode-name "JS")))
    (add-hook 'js2-mode-hook (lambda ()
                               (js2-mode-hide-warnings-and-errors)))
    (dolist (hook '(js2-mode-hook js-mode-hook json-mode-hook))
      (add-hook hook 'rainbow-delimiters-mode)))
  :config
  (js2-imenu-extras-setup))

(use-package skewer-mode
  :ensure t
  :commands (run-skewer skewer-mode)
  :init
  (add-hook 'js2-mode-hook 'skewer-mode)
  (add-hook 'css-mode-hook 'skewer-css-mode)
  (add-hook 'html-mode-hook 'skewer-html-mode))

(use-package js2-refactor
  :ensure t
  :defer t
  :init
  (add-hook 'js2-mode-hook (lambda ()
                             (require 'js2-refactor)))
  :config
  (js2r-add-keybindings-with-prefix "C-c C-m"))

(use-package emmet-mode
  :ensure t
  :commands (emmet-mode)
  :init
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook  'emmet-mode)
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indentation 2)))
  (setq emmet-move-cursor-between-quotes t))

(use-package coffee-mode
  :ensure t
  :defer t
  :mode ("\\.coffee\\'" . coffee-mode)
  :init
  (custom-set-variables '(coffee-tab-width 2)))



;;;; Scala

(use-package scala-mode2
  :ensure t
  :mode ("\\.scala\\'" . scala-mode)
  :init
  (setq scala-indent:align-parameters t
        scala-indent:align-forms t
        scala-indent:indent-value-expression t)
  :config
  (setq scala-indent:default-run-on-strategy scala-indent:operator-strategy))

(use-package sbt-mode
  :ensure t
  :defer t
  :init
  (progn
    (add-hook 'sbt-mode-hook
              '(lambda ()
                 (local-set-key (kbd "C-a") 'comint-bol)
                 (local-set-key (kbd "M-RET") 'comint-accumulate)))

    (add-hook 'scala-mode-hook
              '(lambda ()
                 (local-set-key (kbd "M-.") 'sbt-find-definitions)
                 (local-set-key (kbd "C-x '") 'sbt-run-previous-command)))))


(use-package ensime
  :ensure t
  :commands (ensime ensime-mode)
  :init
  (progn
    (add-hook 'scala-mode-hook 'ensime-scala-mode-hook)
    (with-eval-after-load 'flycheck
      (add-hook 'ensime-mode-hook (lambda ()
                                    (flycheck-mode -1)))))
  :config
  (setq user-emacs-ensime-directory ".cache/ensime"))



;;;; Haskell

(use-package haskell-mode
  :ensure t
  :defer t
  :init
  (add-hook 'haskell-mode-hook 'haskell-indentation-mode)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode))



;; spaceline should be defined after all it's dependencies

(use-package spaceline
  :ensure t
  :config
  (progn
    (setq-default spaceline-window-numbers-unicode t)
    (require 'spaceline-config)
    (spaceline-install
     '(((workspace-number window-number)
        :separator "|"
        :face highlight-face)
       ((point-position
         line-column)
        :separator " | ")
       (buffer-modified buffer-id remote-host)
       anzu
       major-mode
       ((flycheck-error flycheck-warning flycheck-info)
        :when active)
       (((minor-modes :separator spaceline-minor-modes-separator)
         process)
        :when active)
       (version-control :when active))

     `(selection-info
       (global :when active)
       buffer-position))))



;; miscellaneous

;; profile emacs
(use-package esup
  :ensure t
  :commands (esup))



;;;; Key bindings

(use-package init-keybindings
  :bind (("M-Z" . zap-up-to-char)
         ("C-." . set-mark-command)
         ("C-x C-." . pop-global-mark)

         ("C-c C-p" . md/duplicate-down)
         ("C-c C-P" . md/duplicate-up)

         ("C-c b" . switch-to-previous-buffer)
         ("C-c s" . cycle-spacing)
         ("C-c z" . zap-up-to-char)
         ("C-c j"  . join-line)
         ("C-c J" . my-join-line-below)
         ("C-c SPC" . ace-jump-mode)
         ("C-c b" . switch-to-previous-buffer)
         ("C-c f" . find-file)
         ("C-c F" . find-file-other-window)
         ("C-c o" . find-file-other-window)
         ("C-c q" . save-buffers-kill-terminal)

         ("C-c h" . split-window-right)
         ("C-c v" . split-window-below)
         ("C-c d" . delete-window))
  :init
  (autoload 'zap-up-to-char "misc")
  (defun my-join-line-below ()
    (interactive)
    (join-line 1))

  (provide 'init-keybindings))

(require 'init-local nil t)

;; ;;; init.el ends here
