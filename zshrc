is_linux() {
    [[ "$OSTYPE" == "linux-gnu" ]] || return 1
}

is_ubuntu() {
    has_cmd lsb_release || return 1
    local os=`echo $(lsb_release -i | cut -d ':' -f2)`
    [[ "Ubuntu" == "$os" ]] || return 1
}

is_fedora() {
    [[ -f /etc/redhat-release ]] || return 1
}

is_mac() {
    [[ "$OSTYPE" == "darwin"* ]] || return 1
}

has_cmd() {
    command -v $1 > /dev/null
}

smd() {
    [ -d "$1" ] || mkdir -p "$1"
}

srm() {
    if is_ubuntu; then
        trash-put $*
    else
        trash $*
    fi
}

sln() {
    if ! [ -e "$1" ]; then
        echo "$1 does not exist, cannot create the link $2"
    else
        if [ -L "$2" ]; then
            srm "$2"
        elif [ -e "$2" ]; then
            echo "$2 exists and not a symbolic link! not creating link"
            return 1
        fi
    fi
    ln -s $1 $2
}

if is_ubuntu; then
    export JAVA_HOME="/usr/lib/jvm/java-8-oracle"
    export JDK_HOME="/usr/lib/jvm/java-8-oracle"
fi

if is_fedora; then
    export JAVA_HOME="/etc/alternatives/java_sdk"
    export JDK_HOME="/etc/alternatives/java_sdk"
fi

if is_linux; then
    export NVM_DIR="$HOME/.nvm"
    [ -s "$HOME/.nvm/nvm.sh" ] && source "$HOME/.nvm/nvm.sh"
fi

if is_mac; then
    export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
    export JDK_HOME=`/usr/libexec/java_home -v 1.8`

    has_cmd npm && export PATH=$(npm config get prefix)/bin:$PATH
fi

export PATH="$HOME/bin:$PATH"

#haskell
export PATH="$HOME/.cabal/bin:$PATH"

[[ $- = *i* ]] || return 0

# code for interactive shell only

[ -d ~/.slimzsh ] || git clone --recursive https://github.com/changs/slimzsh.git ~/.slimzsh

if [[ -e ~/.slimzsh/slim.zsh ]] && ! [ -n "$INSIDE_EMACS" ]; then
    source "$HOME/.slimzsh/slim.zsh"
else
    autoload -U compinit promptinit
    compinit
    promptinit
    if [ -n "$INSIDE_EMACS" ]; then
        export DISABLE_AUTO_TITLE=true
        prompt adam1
    else
        prompt walters
    fi
fi

unsetopt correct_all

if is_ubuntu; then
    alias trash=trash-put
    alias upgrade="sudo apt-get update && sudo apt-get upgrade -y"
fi

if is_fedora; then
    alias upgrade="sudo dnf -y upgrade"
fi

if is_linux; then
    alias pbcopy="xsel --clipboard --input"
    alias pbpaste="xsel --clipboard --output"
    alias open="xdg-open"
fi

if is_mac; then
    alias upgrade="brew update && brew upgrade"
fi

seartipy_upgrade_dotfiles() {
    local dir=`pwd`
    cd ~/seartipy/lean-dotfiles && git pull --ff-only
    cd $dir
}

seartipy_upgrade_all() {
    upgrade
    seartipy_upgrade_dotfiles
    has_cmd lein && lein
    has_cmd sbt && sbt console -batch
    has_cmd cabal && cabal update
    has_cmd npm && npm update -g
}

seartipy_ctrl_alt_set() {
    if is_linux && has_cmd gnome-session && has_cmd gsettings; then
        gsettings set org.gnome.desktop.input-sources xkb-options "['caps:ctrl_modifier', 'ctrl:lctrl_meta']"
    else
        echo "this command works only in gnome 3"
    fi
}

seartipy_ctrl_alt_reset() {
    if is_linux && has_cmd gnome-session && has_cmd gsettings; then
        gsettings set org.gnome.desktop.input-sources xkb-options "@as []"
    else
        echo "this command works only in gnome 3"
    fi
}

seartipy_fix_links() {
    sln ~/seartipy/lean-dotfiles/zshrc ~/.zshrc

    smd ~/.lein
    smd ~/.boot
    sln ~/seartipy/lean-dotfiles/lein-profiles.clj ~/.lein/profiles.clj
    sln ~/seartipy/lean-dotfiles/boot-profile.boot ~/.boot/profile.boot

    sln ~/seartipy/lean-dotfiles/sbt-plugins.sbt ~/.sbt/0.13/plugins/plugins.sbt

    sln ~/seartipy/lean-dotfiles/spacemacs ~/.spacemacs
    sln ~/seartipy/emacses/spacemacs ~/.emacs.d
}

seartipy_emacs_default() {
    sln "$HOME/seartipy/emacses/$1" ~/.emacs.d
}

seartipy_emacs_launch() {
    seartipy_emacs_default $*
    is_mac && open -a /Applications/Emacs.app
    is_linux && emacs&
}

alias upa=seartipy_upgrade_all

alias sfl=seartipy_fix_links

alias fix-links=seartipy_fix_links
alias ctrl-alt=seartipy_ctrl_alt_set
alias ctrl-alt-reset=seartipy_ctrl_alt_reset

alias cas=seartipy_ctrl_alt_set
alias car=seartipy_ctrl_alt_reset

alias sle="seartipy_emacs_default lean-emacs"
alias sse="seartipy_emacs_default spacemacs"

alias es="seartipy_emacs_launch spacemacs"
alias el="seartipy_emacs_launch lean-emacs"
