This repository consists of simpler configuration for zsh, emacs, clojure, scala, haskell and web development.

### Installation

#### Automated install

Automated installer script will work only with the latest Ubuntu and Mac operating systems. There is little to no chance that it will work with older operating system versions.

##### Mac

1. **Clojure**

		curl -L http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom clojure

2. **Scala**

		curl -L http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom scala

3. **Web development**

		curl -L http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom web

4. **Haskell development**

				curl -L http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom web

5. **Everything**

		curl -L http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom clojure scala web

##### Linux

1. **Clojure**

        wget -qO- http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom clojure

2. **Scala**

		wget -qO- http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom scala

3. **Web development**

		wget -qO- http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom web

4. **Everything**

		wget -qO- http://bit.ly/1NXq6Pp > ~/setup && bash ~/setup zsh emacs atom clojure scala web haskell

**Please Note** :

* No files will be deleted by the installer. All your current files will be backed up to ~/seartipy.backups directory.

* If ~/seartipy.backups already exists, it will be moved to trash. Also, if you find a file deleted but do not find it in ~/seartipy.backups, you should find it in  your trash folder.
