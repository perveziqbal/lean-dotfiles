#! /bin/bash

log() {
    echo "SEARTIPY : $1" | tee ~/seartipy-installer.log
}

warn() {
    echo "WARNING : $1" | tee ~/seartipy-installer.log
}

err_exit() {
    echo "FATAL: $1"
    exit 1
}

is_ubuntu() {
    has_cmd lsb_release || return 1
    local os=`echo $(lsb_release -i | cut -d ':' -f2)`
    [[ "Ubuntu" == "$os" ]] || return 1
}

is_mac() {
    [[ "$OSTYPE" == "darwin"* ]] || return 1
}

has_cmd() {
    command -v $1 > /dev/null
}

smd() {
    [ -d "$1" ] || mkdir -p "$1" 2> /dev/null
}

srm() {
    $trash $1 2> /dev/null
}

smv() {
    mv $1 $2 2> /dev/null
}

sln() {
    if ! [ -e "$1" ]; then
        warn "$1 does not exist, cannot create the link $2"
    else
        if [ -L "$2" ]; then
            srm "$2"
        elif [ -e "$2" ]; then
            warn "$2 exists and not a symbolic link! not creating link"
            return
        fi
    fi
    ln -s $1 $2
}

fln() {
    if [ -e "$1" ]; then
        srm "$2"
    else
        warn "$1 does not exist, cannot create the link $2"
    fi
    ln -s $1 $2
}

pre_cmd_check() {
    for cmd in $*; do
        has_cmd "$cmd" || err_exit "$cmd not be installed, quitting"
    done
}

pre_dir_check() {
    for dir in $*; do
        [ -d "$dir" ] || err_exit "$dir does not exist, quitting"
    done
}

cmd_check() {
    for cmd in $*; do
        has_cmd "$cmd" || warn "$cmd not installed"
    done
}

dir_check() {
    for dir in $*; do
        [ -d "$dir" ] || warn "$dir does not exist"
    done
}

ln_check() {
    local rl=readlink
    is_mac && rl=greadlink
    [[ "$1" == $($rl -f $2) ]] ||  warn "$2 not a link to $1"
}

sclone() {
    local dest=${*: -1}
    local src=${*: -2:1}

    if [ -d "$dest" ]; then
        cd "$dest" && git pull --ff-only
        cd
    else
        log "Cloning $src to $dest"
        git clone $*
    fi
}

fclone() {
    [ -d "$2" ] && srm "$2"
    log "Cloning $1 to $2"
    git clone $1 $2
}

# doesnt work if the system sleeps; perhaps better to disable sleep before the installer starts,
# and restore it when the script exits.
keep_sudo_running() {
    # Ask for the administrator password upfront
    sudo -v
    # Keep-alive: update existing `sudo` time stamp until this script has finished
    while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2> /dev/null &
}

install() {
    if is_ubuntu; then
        sudo apt-get install -y $* || err_exit "apt-get($*) failed, quitting"
    elif is_mac; then
        brew install $* || err_exit "brew($*) failed, quitting"
    fi
}

linux_fasd_install() {
    is_ubuntu || return 1
    has_cmd fasd && return 0
    has_cmd make || return 1

    log "Installing fasd..."
    srm ~/seartipy-installer-fasd
    if git clone https://github.com/clvv/fasd.git ~/seartipy-installer-fasd; then
        cd ~/seartipy-installer-fasd && PREFIX=$HOME make install
        cd
        srm ~/seartipy-installer-fasd
    fi
}

apmi() {
    has_cmd apm || return 1

    for p in $*; do
        apm ls --installed --bare | cut -d "@" -f 1 | grep "^$p$" > /dev/null || apm install $p
    done
}

npmi() {
    has_cmd npm || return 1

    for p in $*; do
        npm list -g $p &> /dev/null || npm install -g $p
    done
}

add_atom_ppa() {
    is_ubuntu || return 1

    ls /etc/apt/sources.list.d | grep webupd8team-ubuntu-atom > /dev/null && return 1

    log "Adding atom ppa"
    sudo add-apt-repository ppa:webupd8team/atom -y
}

add_java_ppa() {
    is_ubuntu || return 1
    ls /etc/apt/sources.list.d | grep webupd8team-ubuntu-java > /dev/null && return 1
    pre_cmd_check debconf-set-selections

    log "Adding java ppa..."
    echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
    sudo add-apt-repository ppa:webupd8team/java -y
}

add_scala_ppa() {
    is_ubuntu || return 1
    ls /etc/apt/sources.list.d | grep sbt > /dev/null && return 1
    pre_cmd_check apt-key

    log "Adding scala ppa..."
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
    echo "deb http://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
}

ubuntu_update() {
    is_ubuntu || return 1
    log "Updating ubuntu..."
    sudo apt-get update || err_exit "apt-get update failed, quitting"
}

ubuntu_upgrade() {
    is_ubuntu || return 1
    log "Upgrading packages..."
    sudo apt-get upgrade -y || err_exit "apt-get upgrade failed, quitting"
}

add_ppas() {
    add_java_ppa
    add_scala_ppa
    add_atom_ppa
}

ubuntu_essential_install() {
    add_ppas
    ubuntu_update
    ubuntu_upgrade

    is_ubuntu || return 1

    install curl wget git trash-cli tree xsel xclip silversearcher-ag unrar build-essential
    linux_fasd_install
}

brew_update() {
    is_mac || return 1
    brew update
}

brew_upgrade() {
    is_mac || return 1
    brew upgrade
}

brew_install() {
    is_mac || return 1
    has_cmd brew && return 1
    pre_cmd_check ruby

    log "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    pre_cmd_check "could not install brew, quitting"

    brew install caskroom/cask/brew-cask # caskroom
}

mac_essential_install() {
    brew_install
    brew_update
    brew_upgrade

    is_mac || return 1

    install wget trash tree fasd unar gpg the_silver_searcher coreutils
    brew cask install iterm2
}

essential_install() {
    ubuntu_essential_install
    mac_essential_install
}

spacemacs_dotfiles() {
    log "Cloning spacemacs to ~/seartipy/emacses/spacemacs..."
    sclone https://github.com/syl20bnr/spacemacs ~/seartipy/emacses/spacemacs
    fln ~/seartipy/lean-dotfiles/spacemacs ~/.spacemacs
    fln ~/seartipy/emacses/spacemacs ~/.emacs.d
}

leanemacs_dotfiles() {
    smd ~/seartipy/emacses/lean-emacs
    fln ~/seartipy/lean-dotfiles/emacs-init.el ~/seartipy/emacses/lean-emacs/init.el
}

emacs_dotfiles() {
    leanemacs_dotfiles
    spacemacs_dotfiles
}

ubuntu_emacs_install() {
    is_ubuntu || return 1
    install emacs24 aspell aspell-en editorconfig exuberant-ctags
}

mac_emacs_install() {
    is_mac || return 1

    brew tap railwaycat/emacsmacport
    install emacs-mac ---with-spacemacs-icon
    install editorconfig
    # TODO : ctags
    install aspell --with-lang-en

    log "Trashing /Applications/Emacs.app alias (if exists)..."
    srm '/Applications/Emacs.app alias'

    log "Creating Emacs alias in /Applications..."
    # make emacs available from spotlight
    osascript -e 'tell application "Finder" to make alias file to POSIX file "/usr/local/opt/emacs-mac/Emacs.app" at POSIX file "/Applications"'

    log "Generating terminfo for terminal in emacs..."
    local emacsversion=`ls -r /usr/local/Cellar/emacs-mac | head -1`
    tic -o ~/.terminfo /usr/local/Cellar/emacs-mac/${emacsversion}/share/emacs/24.5/etc/e/eterm-color.ti
}

emacs_install() {
    ubuntu_emacs_install
    mac_emacs_install

    emacs_dotfiles
}

ubuntu_atom_install() {
    is_ubuntu || return 1
    install atom
}

mac_atom_install() {
    is_mac || return 1
    brew cask install atom
}

atom_install() {
    ubuntu_atom_install
    mac_atom_install

    apmi file-icons linter atom-beautify terminal-plus jumpy hyperclick project-manager last-cursor-position git-plus atomatigit blame merge-conflicts git-history language-diff script atom-fix-path
}

ubuntu_zsh_install() {
    is_ubuntu || return 1
    install zsh
}

zsh_dotfiles() {
    fln ~/seartipy/lean-dotfiles/zshrc ~/.zshrc
}

zsh_install() {
    ubuntu_zsh_install

    sclone --recursive https://github.com/changs/slimzsh.git ~/.slimzsh
    zsh_dotfiles
}

ubuntu_java_install() {
    is_ubuntu || return 1
    install oracle-java8-installer oracle-java8-set-default
}

mac_java_install() {
    is_mac || return 1
    brew cask install java
}

java_install() {
    ubuntu_java_install
    mac_java_install
}

ubuntu_scala_install() {
    is_ubuntu || return 1
    install scala sbt
}

mac_scala_install() {
    is_mac || return 1
    install scala sbt
}

scala_dotfiles() {
    smd $HOME/.sbt/0.13/plugins
    fln ~/seartipy/lean-dotfiles/sbt-plugins.sbt ~/.sbt/0.13/plugins/plugins.sbt
}

scala_install() {
    java_install

    ubuntu_scala_install
    mac_scala_install

    scala_dotfiles

    apmi language-scala ensime
}

ubuntu_haskell_install() {
    is_ubuntu || return 1
    install ghc cabal-install
}

mac_haskell_install() {
    is_mac || return 1
    install ghc cabal-install
}

haskell_install() {
    ubuntu_haskell_install
    mac_haskell_install

    has_cmd cabal || return 1

    export PATH="$HOME/.cabal/bin:$PATH"

    log "Installing cabal packages"
    cabal update
    cabal install alex happy
    cabal install hlint
    cabal install structured-haskell-mode
    cabal install stylish-haskell
    cabal install hasktags
    cabal install hindent
    cabal install hdevtools
    cabal install ghc-mod

    apmi language-haskell ide-haskell haskell-ghc-mod autocomplete-haskell ide-haskell-repl ide-haskell-cabal

}

mac_clojure_install() {
    is_mac || return 1
    install leiningen boot-clj
}

lein_dotfiles() {
    smd ~/.lein
    fln ~/seartipy/lean-dotfiles/lein-profiles.clj ~/.lein/profiles.clj
}

boot_dotfiles() {
    smd ~/.boot
    fln ~/seartipy/lean-dotfiles/boot-profile.boot ~/.boot/profile.boot
}

linux_boot_install() {
    is_ubuntu || return 1
    has_cmd boot || curl -L https://github.com/boot-clj/boot-bin/releases/download/2.4.2/boot.sh > ~/bin/boot
    chmod a+x ~/bin/boot
    boot -u
}

linux_lein_install() {
    is_ubuntu || return 1
    has_cmd lein || curl -L https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > ~/bin/lein
    chmod a+x ~/bin/lein
}

clojure_install() {
    java_install

    linux_boot_install
    linux_lein_install

    mac_clojure_install

    lein_dotfiles
    boot_dotfiles

    apmi Parinfer proto-repl linter-clojure lisp-paredit
}

mac_node_install() {
    is_mac || return 1
    install node
}

linux_node_install() {
    is_ubuntu || return 1

    [ -s ~/.nvm/nvm.sh ] && source ~/.nvm/nvm.sh
    has_cmd nvm || curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash

    [ -s ~/.nvm/nvm.sh ] && source ~/.nvm/nvm.sh
    has_cmd nvm || return 1

    if ! has_cmd node; then
        nvm install stable
        nvm alias default stable
    fi
}

web_install() {
    linux_node_install
    mac_node_install

    log "Installing npm packages for web development..."
    npmi budo browserify babel webpack karma-cli eslint eslint-plugin-react tern js-beautify webpack-dev-server live-server

    apmi language-babel linter-eslint emmet
}

ubuntu_cleanup() {
    is_ubuntu || return 1

    sudo apt-get autoremove -y
    sudo apt-get clean -y
    sudo apt-get autoclean -y
}

mac_cleanup() {
    is_mac || return 1
    brew cleanup
    brew cask cleanup
}

cleanup() {
    ubuntu_cleanup
    mac_cleanup
}

clone_dotfiles() {
    log "Backing up your current lean-dotfiles(if exists) to $BACKUP_DIR..."
    sclone https://perveziqbal@bitbucket.org/perveziqbal/lean-dotfiles.git ~/seartipy/lean-dotfiles
    [ -n "$SEARTIPY" ] && cd ~/seartipy/lean-dotfiles && git checkout develop
    cd
}

script_options() {
    while [[ $# > 0 ]]; do
        case $1 in
            clojure)
                CLOJURE="clojure"
                shift
                ;;
            scala)
                SCALA="scala"
                shift
                ;;
            web)
                WEB="web"
                shift
                ;;
            atom)
                ATOM="atom"
                shift
                ;;
            haskell)
                HASKELL="haskell"
                shift
                ;;
            zsh)
                ZSH="zsh"
                shift
                ;;
            emacs)
                EMACS="emacs"
                shift
                ;;
            fresh)
                FRESH="fresh"
                shift
                ;;
            diagnostics)
                DIAGNOSTICS="diagnostics"
                shift
                ;;
            *)
                shift # ignore unknown option
                ;;
        esac
    done
}

setup_backup_dir() {
    BACKUP_DIR="$HOME/seartipy.backups"

    if [ -d $BACKUP_DIR ]; then
        log "moving $BACKUP_DIR to trash..."
        srm $BACKUP_DIR
    fi

    smd $BACKUP_DIR
}

create_dirs() {
    setup_backup_dir
    smd $HOME/bin
    smd $HOME/seartipy/emacses
}

fresh_install() {
  has_cmd $trash || err_exit "no $trash, cannot fresh install, quitting"
  cd
  sudo $trash /etc/apt/sources.list.d/* &> /dev/null # essential
  $trash ~/.zshrc ~/.slimzsh &> /dev/null
  $trash ~/.nvm ~/.npm ~/.node-gyp &> /dev/null # node
  $trash ~/.lein ~/.boot ~/.m2 &> /dev/null # clojure
  $trash ~/seartipy ~/bin &> /dev/null
  smd ~/bin
  $trash ~/.sbt ~/.ivy2 &> /dev/null # scala
  $trash ~/.ghc ~/.cabal ~/.stack&> /dev/null # haskell
  $trash ~/.emacs.d ~/.spacemacs &> /dev/null # emacs
  $trash /Applications/Emacs.app alias
  $trash ~/.apm &> /dev/null # atom
}

installer() {
    script_options $*

    if [ -n "$DIAGNOSTICS" ]; then
        post_installer_check
        exit 0
    fi

    [ -n "$FRESH" ] && fresh_install

    log "Installing $ZSH $EMACS $ATOM $SCALA $WEB $CLOJURE $HASKELL..."

    essential_install

    pre_installer_check

    clone_dotfiles

    [ -n "$ZSH" ] && zsh_install
    [ -n "$EMACS" ] && emacs_install
    [ -n "$ATOM" ] && atom_install

    [ -n "$CLOJURE" ] && clojure_install
    [ -n "$SCALA" ] && scala_install
    [ -n "$WEB" ] && web_install
    [ -n "$HASKELL" ] && haskell_install

    if [ -n "$ZSH" ]; then
        log "Set zsh as your default shell(this sometimes fails)..."
        chsh -s /bin/zsh
    fi

    cleanup

    post_installer_check

    log "Installation done!!! You could run after rebooting your computer"
    log "bash ~/seartipy/lean-dotfiles/installer.sh diagnostics $ZSH $EMACS $ATOM $CLOJURE $SCALA $WEB $HASKELL"
    log "to check your installation"
}

pre_installer_check() {
    pre_cmd_check git curl wget unzip make $trash
    pre_dir_check "$BACKUP_DIR" ~/bin
}

ppas_check() {
    is_ubuntu || return 1

    ls /etc/apt/sources.list.d | grep webupd8team-ubuntu-java > /dev/null || warn 'java ppa not added'
    ls /etc/apt/sources.list.d | grep sbt > /dev/null || warn 'sbt ppa not added'
    ls /etc/apt/sources.list.d | grep webupd8team-ubuntu-atom > /dev/null || warn 'atom ppa not added'
}

essential_check() {
    ppas_check
    cmd_check curl wget git $trash tree fasd ag unrar
    is_ubuntu && cmd_check xsel xclip
    is_mac && cmd_check unar gpg

    dir_check ~/seartipy/lean-dotfiles
}

emacs_check() {
    cmd_check emacs aspell ctags
    is_ubuntu && cmd_check editorconfig
    is_mac && cmd_check editorconfig

    ln_check ~/seartipy/lean-dotfiles/emacs-init.el ~/seartipy/emacses/lean-emacs/init.el

    ln_check ~/seartipy/lean-dotfiles/spacemacs ~/.spacemacs
    ln_check ~/seartipy/emacses/spacemacs ~/.emacs.d
}

atom_check() {
    cmd_check atom apm
}

zsh_check() {
    cmd_check zsh

    dir_check ~/.slimzsh
    ln_check ~/seartipy/lean-dotfiles/zshrc ~/.zshrc
}

java_check() {
    cmd_check javac
}

scala_check() {
    java_check
    cmd_check scala sbt

    ln_check ~/seartipy/lean-dotfiles/sbt-plugins.sbt ~/.sbt/0.13/plugins/plugins.sbt
}

haskell_check() {
    cmd_check ghc cabal
    cmd_check alex happy hlint structured-haskell-mode stylish-haskell hasktags hdevtools
}

clojure_check() {
    java_check
    cmd_check lein boot

    ln_check ~/seartipy/lean-dotfiles/lein-profiles.clj ~/.lein/profiles.clj
    ln_check ~/seartipy/lean-dotfiles/boot-profile.boot ~/.boot/profile.boot
}

web_check() {
    is_mac && export PATH=$(npm config get prefix)/bin:$PATH
    is_ubuntu && source ~/.nvm/nvm.sh

    cmd_check npm
    is_ubuntu && cmd_check nvm

    has_cmd npm || return 1
    cmd_check budo browserify babel webpack eslint tern js-beautify webpack-dev-server
}

post_installer_check() {
    log "Running diagnostics..."

    essential_check
    [ -n "$EMACS" ] && emacs_check
    [ -n "$ATOM" ] && atom_check
    [ -n "$ZSH" ] && zsh_check
    [ -n "$CLOJURE" ] && clojure_check
    [ -n "$SCALA" ] && scala_check
    [ -n "$WEB" ] && web_check
    [ -n "$HASKELL" ] && haskell_check

    if is_ubuntu && [ -n "$ZSH" ]; then
        cat /etc/passwd | grep $USER | grep zsh > /dev/null ||  warn "zsh not your default shell"
    fi
    log "diagnostics done!"
}


curdir=`pwd`

try_trash() {
    if has_cmd trash; then
        trash $* 2> /dev/null
    elif has_cmd trash-put; then
        trash-put $* 2> /dev/null
    else
        rm -rf $*
    fi
}
try_trash ~/seartipy-installer.log ~/seartipy-error.log ~/seartipy-output.log

keep_sudo_running

create_dirs

export PATH="$HOME/bin:$PATH"

if is_ubuntu; then
    trash=trash-put
else
    trash=trash
fi

if [[ $- = *i* ]]; then
    log "Running pre installer check..."
    log "if this does not succeed you must first at least install essentials by running this script without parameters"
    pre_installer_check
    log "installer check succeeded, you could run functions in this script for testing"
else
    installer $* > >(tee ~/seartipy-output.log) 2> >(tee ~/seartipy-error.log >&2)
fi

cd $curdir
