{:user {:plugins [[refactor-nrepl "2.0.0-SNAPSHOT"] ; refactor in emacs using clj-refactor
                  [lein-midje "3.2"] ; midje plugin for continous testing
                  [quickie "0.4.1"]] ; lein plugin for auto testing
        :dependencies [[org.clojure/tools.nrepl "0.2.11"]]}
 :repl {:plugins [[cider/cider-nrepl "0.11.0-SNAPSHOT"]]}} ; nrepl server for cider in emacs
